# Draw.io Docker Image

Draw.io is a tool for drawing diagrams in a web browser. This repo is a Docker image that hosts Draw.io on an Nginx web server.

## Example Run Command

```
docker run --name draw.io -d -p 80:80 katharostech/draw.io
```
