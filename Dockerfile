FROM alpine

# Update OS
RUN apk upgrade --no-cache

# Install dependencies
RUN apk add --no-cache \
  ca-certificates \
  nginx \
  unzip \
  wget

# Create required nginx run directory
RUN mkdir -p /run/nginx

# Remove default nginx config
RUN rm /etc/nginx/nginx.conf
RUN rm /etc/nginx/conf.d/default.conf

# Copy in our nginx config
COPY draw.io.conf /etc/nginx/conf.d/draw.io.conf
COPY nginx.conf /etc/nginx/nginx.conf

# Create the html directory
RUN mkdir -p /usr/share/nginx/html

# Collect the Draw.io version to download from github as a build argument
# and save it in an environment variable
ARG DRAWIO_VERSION
ENV DRAWIO_VERSION=$DRAWIO_VERSION

# Download and install the draw.io war from the github release
RUN \
  cd /usr/share/nginx/html && \
  wget https://github.com/jgraph/draw.io/releases/download/v${DRAWIO_VERSION}/draw.war && \
  unzip draw.war && \
  rm draw.war

# Copy in our Docker cmd
COPY docker-cmd.sh /docker-cmd.sh
RUN chmod 744 /docker-cmd.sh

# Copy in the container-start and container-stop scripts
COPY start-container.sh /start-container.sh
RUN chmod 744 /start-container.sh

COPY stop-container.sh /stop-container.sh
RUN chmod 744 /stop-container.sh

# Set the Docker command
CMD ["/docker-cmd.sh"]
